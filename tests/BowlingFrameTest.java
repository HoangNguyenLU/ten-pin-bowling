import org.junit.Test;

import static org.junit.Assert.*;

public class BowlingFrameTest {

    @Test
    public void testThrowBall() {
        // Score a Spare without knowing the next ball
        BowlingFrame spareFrame = new BowlingFrame();
        spareFrame.throwBall(new BowlingBall(7));
        spareFrame.throwBall(new BowlingBall(3));
        assertEquals(true, spareFrame.isSpareScored());
        assertEquals(false, spareFrame.isStrikeScored());
        assertEquals(10, spareFrame.getFrameScore());

        // Score a Strike without knowing the next two balls
        BowlingFrame strikeFrame = new BowlingFrame();
        strikeFrame.throwBall(new BowlingBall(10));
        assertEquals(false, strikeFrame.isSpareScored());
        assertEquals(true, strikeFrame.isStrikeScored());
        assertEquals(10, strikeFrame.getFrameScore());

        // Miss both balls
        BowlingFrame missBothFrame = new BowlingFrame();
        missBothFrame.throwBall(new BowlingBall(0));
        missBothFrame.throwBall(new BowlingBall(0));
        assertEquals(false, missBothFrame.isSpareScored());
        assertEquals(false, missBothFrame.isStrikeScored());
        assertEquals(0, missBothFrame.getFrameScore());

        // Throw two balls, no miss, no strike, no spare
        BowlingFrame throwTwoNormalBallsFrame = new BowlingFrame();
        throwTwoNormalBallsFrame.throwBall(new BowlingBall(3));
        throwTwoNormalBallsFrame.throwBall(new BowlingBall(4));
        assertEquals(false, throwTwoNormalBallsFrame.isSpareScored());
        assertEquals(false, throwTwoNormalBallsFrame.isStrikeScored());
        assertEquals(7, throwTwoNormalBallsFrame.getFrameScore());
    }



    @Test
    public void testInitializeFrameWithString() {
        // Score a Spare without knowing the next ball
        BowlingFrame spareFrame = new BowlingFrame();
        spareFrame.initializeFrameWithString("5" + TenPinBowlingConstants.SPARE);
        assertEquals(true, spareFrame.isSpareScored());
        assertEquals(false, spareFrame.isStrikeScored());
        assertEquals(10, spareFrame.getFrameScore());

        // Score a Strike without knowing the next two balls
        BowlingFrame strikeFrame = new BowlingFrame();
        strikeFrame.initializeFrameWithString(TenPinBowlingConstants.STRIKE);
        assertEquals(false, strikeFrame.isSpareScored());
        assertEquals(true, strikeFrame.isStrikeScored());
        assertEquals(10, strikeFrame.getFrameScore());

        // Miss both balls
        BowlingFrame missBothFrame = new BowlingFrame();
        missBothFrame.initializeFrameWithString(TenPinBowlingConstants.MISS + TenPinBowlingConstants.MISS);
        assertEquals(false, missBothFrame.isSpareScored());
        assertEquals(false, missBothFrame.isStrikeScored());
        assertEquals(0, missBothFrame.getFrameScore());

        // Miss first ball
        BowlingFrame missFirstFrame = new BowlingFrame();
        missFirstFrame.initializeFrameWithString(TenPinBowlingConstants.MISS + "4");
        assertEquals(false, missFirstFrame.isSpareScored());
        assertEquals(false, missFirstFrame.isStrikeScored());
        assertEquals(4, missFirstFrame.getFrameScore());

        // Miss second ball
        BowlingFrame missSecondFrame = new BowlingFrame();
        missSecondFrame.initializeFrameWithString("5" + TenPinBowlingConstants.MISS);
        assertEquals(false, missSecondFrame.isSpareScored());
        assertEquals(false, missSecondFrame.isStrikeScored());
        assertEquals(5, missSecondFrame.getFrameScore());

        // Throw two balls, no miss, no strike, no spare
        BowlingFrame throwTwoNormalBallsFrame = new BowlingFrame();
        throwTwoNormalBallsFrame.initializeFrameWithString("34");
        assertEquals(false, throwTwoNormalBallsFrame.isSpareScored());
        assertEquals(false, throwTwoNormalBallsFrame.isStrikeScored());
        assertEquals(7, throwTwoNormalBallsFrame.getFrameScore());
    }
}