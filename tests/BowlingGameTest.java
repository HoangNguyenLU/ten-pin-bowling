import org.junit.Test;

import static org.junit.Assert.*;

public class BowlingGameTest {

    @Test
    public void testInitializeGameWithString() {
        // Test cases given by the problem
        BowlingGame newGame = new BowlingGame();
        newGame.initializeGameWithString("5/|5/|5/|5/|5/|5/|5/|5/|5/|5/||5");
        assertEquals(150, newGame.getGameScore());

        BowlingGame newGame1 = new BowlingGame();
        newGame1.initializeGameWithString("X|X|X|X|X|X|X|X|X|X||XX");
        assertEquals(300, newGame1.getGameScore());

        BowlingGame newGame2 = new BowlingGame();
        newGame2.initializeGameWithString("X|7/|9-|X|-8|8/|-6|X|X|X||81");
        assertEquals(167, newGame2.getGameScore());

        BowlingGame newGame3 = new BowlingGame();
        newGame3.initializeGameWithString("9-|9-|9-|9-|9-|9-|9-|9-|9-|9-||");
        assertEquals(90, newGame3.getGameScore());

        BowlingGame newGame4 = new BowlingGame();
        newGame4.initializeGameWithString("--|--|--|--|--|--|--|--|--|9/||5");
        assertEquals(15, newGame4.getGameScore());

        BowlingGame newGame5 = new BowlingGame();
        newGame4.initializeGameWithString("--|--|--|--|--|--|--|--|--|--||");
        assertEquals(0, newGame5.getGameScore());
    }

    @Test
    public void testInitializeBonusBallWithString() {
        // Test cases given by the problem
        BowlingGame newGame = new BowlingGame();
        newGame.setUpBonusBallsWithString("81");
        assertEquals(9, newGame.getBonusPoint());

        BowlingGame newGame1 = new BowlingGame();
        newGame1.setUpBonusBallsWithString("XX");
        assertEquals(20, newGame1.getBonusPoint());

        BowlingGame newGame3 = new BowlingGame();
        newGame3.setUpBonusBallsWithString("8");
        assertEquals(8, newGame3.getBonusPoint());

        // Score a spare with two bonus balls
        BowlingGame newGame2 = new BowlingGame();
        newGame2.setUpBonusBallsWithString("7/");
        assertEquals(10, newGame2.getBonusPoint());
    }

    @Test
    public void testThrowBonusBall() {
        // No bonus ball at all
        BowlingGame noBonusBallGame = new BowlingGame();
        assertEquals(0, noBonusBallGame.getBonusPoint());
        assertEquals(0, noBonusBallGame.getAllBonusBallsPlayed().size());

        // Throw one bonus ball, scored a strike
        BowlingGame oneBonusBallStrikeGame = new BowlingGame();
        oneBonusBallStrikeGame.throwBonusBall(new BowlingBall(10));
        assertEquals(10, oneBonusBallStrikeGame.getBonusPoint());
        assertEquals(1, oneBonusBallStrikeGame.getAllBonusBallsPlayed().size());

        // Throw two bonus balls, scored two strikes
        BowlingGame twoBonusBallStrikeGame = new BowlingGame();
        twoBonusBallStrikeGame.throwBonusBall(new BowlingBall(10));
        twoBonusBallStrikeGame.throwBonusBall(new BowlingBall(10));
        assertEquals(20, twoBonusBallStrikeGame.getBonusPoint());
        assertEquals(2, twoBonusBallStrikeGame.getAllBonusBallsPlayed().size());

        // Throw two bonus balls, scored a spare
        BowlingGame twoBonusBallSpareGame = new BowlingGame();
        twoBonusBallSpareGame.throwBonusBall(new BowlingBall(5));
        twoBonusBallSpareGame.throwBonusBall(new BowlingBall(5));
        assertEquals(10, twoBonusBallSpareGame.getBonusPoint());

        // Throw one bonus ball, do not score a strike
        BowlingGame oneBonusBallGame = new BowlingGame();
        oneBonusBallGame.throwBonusBall(new BowlingBall(9));
        assertEquals(9, oneBonusBallGame.getBonusPoint());

        // Throw two bonus balls, do not score any strike
        BowlingGame twoBonusBallGame = new BowlingGame();
        twoBonusBallGame.throwBonusBall(new BowlingBall(5));
        twoBonusBallGame.throwBonusBall(new BowlingBall(4));
        assertEquals(9, twoBonusBallGame.getBonusPoint());
    }

    @Test
    public void testAddBowlingFrame() {
        BowlingGame singleFrameGame = new BowlingGame();
        BowlingFrame singleFrame = new BowlingFrame();
        singleFrame.throwBall(new BowlingBall(10));
        singleFrameGame.addBowlingFrame(singleFrame);
        assertEquals(10, singleFrameGame.getGameScore());
        assertEquals(1, singleFrameGame.getAllFramesPlayed().size());
        assertEquals(1, singleFrameGame.getFrameAt(0).getBowlingBalls().size());
        assertEquals(true, singleFrameGame.getFrameAt(0).isStrikeScored());
    }

    @Test
    public void testFullGameAllStrikes() {
        // Ten strikes on the first ball of all ten frames.
        // Two bonus balls, both strikes.
        BowlingGame allStrikesGame = new BowlingGame();
        for(int i = 0; i < 10; i++) {
            BowlingFrame strikeFrame = new BowlingFrame();
            strikeFrame.throwBall(new BowlingBall(10));
            allStrikesGame.addBowlingFrame(strikeFrame);
        }
        allStrikesGame.throwBonusBall(new BowlingBall(10));
        allStrikesGame.throwBonusBall(new BowlingBall(10));
        assertEquals(10, allStrikesGame.getAllFramesPlayed().size());
        assertEquals(1, allStrikesGame.getFrameAt(0).getBowlingBalls().size());
        assertEquals(true, allStrikesGame.getFrameAt(0).isStrikeScored());
        assertEquals(300, allStrikesGame.getGameScore());
    }

    @Test
    public void testFullGameAllSpares() {
        // Five pins on the first ball of all ten frames.
        // Second ball of each frame hits all five remaining pins, a spare.
        // One bonus ball, hits five pins.
        BowlingGame allSparesGame = new BowlingGame();
        for(int i = 0; i < 10; i++) {
            BowlingFrame spareFrame = new BowlingFrame();
            spareFrame.throwBall(new BowlingBall(5));
            spareFrame.throwBall(new BowlingBall(5));
            allSparesGame.addBowlingFrame(spareFrame);
        }
        allSparesGame.throwBonusBall(new BowlingBall(5));
        assertEquals(10, allSparesGame.getAllFramesPlayed().size());
        assertEquals(2, allSparesGame.getFrameAt(0).getBowlingBalls().size());
        assertEquals(true, allSparesGame.getFrameAt(0).isSpareScored());
        assertEquals(false, allSparesGame.getFrameAt(0).isStrikeScored());
        assertEquals(150, allSparesGame.getGameScore());
    }

    @Test
    public void testFullGameAllNormals() {
        // Nine pins hit on the first ball of all ten frames.
        // Second ball of each frame misses last remaining pin.
        // No bonus balls.
        BowlingGame allNormalsGame = new BowlingGame();
        for(int i = 0; i < 10; i++) {
            BowlingFrame ninePointFrame = new BowlingFrame();
            ninePointFrame.throwBall(new BowlingBall(9));
            ninePointFrame.throwBall(new BowlingBall(0));
            allNormalsGame.addBowlingFrame(ninePointFrame);
        }
        assertEquals(10, allNormalsGame.getAllFramesPlayed().size());
        assertEquals(2, allNormalsGame.getFrameAt(0).getBowlingBalls().size());
        assertEquals(false, allNormalsGame.getFrameAt(0).isSpareScored());
        assertEquals(false, allNormalsGame.getFrameAt(0).isStrikeScored());
        assertEquals(90, allNormalsGame.getGameScore());
    }

    @Test
    public void completeNormalGame() {
        // We will test this game: X|7/|9-|X|-8|8/|-6|X|X|X||81
        BowlingGame completeNormalGame = new BowlingGame();

        // Strike
        BowlingFrame frame1 = new BowlingFrame();
        frame1.throwBall(new BowlingBall(10));
        completeNormalGame.addBowlingFrame(frame1);

        // 7/
        BowlingFrame frame2 = new BowlingFrame();
        frame2.throwBall(new BowlingBall(7));
        frame2.throwBall(new BowlingBall(3));
        completeNormalGame.addBowlingFrame(frame2);

        // 9-
        BowlingFrame frame3 = new BowlingFrame();
        frame3.throwBall(new BowlingBall(9));
        frame3.throwBall(new BowlingBall(0));
        completeNormalGame.addBowlingFrame(frame3);

        // Strike
        BowlingFrame frame4 = new BowlingFrame();
        frame4.throwBall(new BowlingBall(10));
        completeNormalGame.addBowlingFrame(frame4);

        // -8
        BowlingFrame frame5 = new BowlingFrame();
        frame5.throwBall(new BowlingBall(0));
        frame5.throwBall(new BowlingBall(8));
        completeNormalGame.addBowlingFrame(frame5);

        // 8/
        BowlingFrame frame6 = new BowlingFrame();
        frame6.throwBall(new BowlingBall(8));
        frame6.throwBall(new BowlingBall(2));
        completeNormalGame.addBowlingFrame(frame6);

        // -6
        BowlingFrame frame7 = new BowlingFrame();
        frame7.throwBall(new BowlingBall(0));
        frame7.throwBall(new BowlingBall(6));
        completeNormalGame.addBowlingFrame(frame7);

        // Strike
        BowlingFrame frame8 = new BowlingFrame();
        frame8.throwBall(new BowlingBall(10));
        completeNormalGame.addBowlingFrame(frame8);

        // Strike
        BowlingFrame frame9 = new BowlingFrame();
        frame9.throwBall(new BowlingBall(10));
        completeNormalGame.addBowlingFrame(frame9);

        // Strike
        BowlingFrame frame10 = new BowlingFrame();
        frame10.throwBall(new BowlingBall(10));
        completeNormalGame.addBowlingFrame(frame10);

        completeNormalGame.throwBonusBall(new BowlingBall(8));
        completeNormalGame.throwBonusBall(new BowlingBall(1));
        assertEquals(10, completeNormalGame.getAllFramesPlayed().size());
        assertEquals(true, completeNormalGame.getFrameAt(7).isStrikeScored());
        assertEquals(true, completeNormalGame.getFrameAt(8).isStrikeScored());
        assertEquals(true, completeNormalGame.getFrameAt(9).isStrikeScored());
        assertEquals(9, completeNormalGame.getBonusPoint());
        assertEquals(167, completeNormalGame.getGameScore());

    }

}