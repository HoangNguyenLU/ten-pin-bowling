import java.util.ArrayList;

/**
 * A full bowling game.
 */
public class BowlingGame {


    //region Properties & Variables

    //Internal variables
    private ArrayList<BowlingFrame> allFramesPlayed;
    private ArrayList<BowlingBall> bowlingBalls;
    private ArrayList<BowlingBall> bonusBalls;
    private int bonusPoint;
    private int gameScore;

    //endregion Properties & Variables


    //region Constructor

    /**
     * Default constructor for a bowling game.
     */
    public BowlingGame() {
        allFramesPlayed = new ArrayList<>();
        bowlingBalls = new ArrayList<>();
        bonusBalls = new ArrayList<>();
        gameScore = 0;
        bonusPoint = 0;
    }

    //endregion Constructor


    //region Public Interface

    /**
     * Get the total bonus point scored by all bonus balls this game.
     * @return bonusPoint
     */
    public int getBonusPoint() {
        return bonusPoint;
    }

    /**
     * Get the score of this game.
     * @return gameScore
     */
    public int getGameScore() {
        return gameScore;
    }

    /**
     * Return the ArrayList of all the bonus balls played this game.
     * @return bonusBalls
     */
    public ArrayList<BowlingBall> getAllBonusBallsPlayed() {
        return bonusBalls;
    }

    /**
     * Return the ArrayList of all the frames played this game.
     * @return allFramesPlayed
     */
    public ArrayList<BowlingFrame> getAllFramesPlayed() {
        return allFramesPlayed;
    }

    /**
     * Return the ArrayList of all the frames played this game.
     * @return
     */
    public BowlingFrame getFrameAt(int index) {
        return allFramesPlayed.get(index);
    }


    /**
     * Set up this bowling game using the given game string.
     *
     * @param gameString
     */
    public void initializeGameWithString(String gameString) {
        String[] wholeGameStringArray = gameString.split(TenPinBowlingConstants.BONUS_BALLS);
        String allFramesString = wholeGameStringArray[0];
        String[] bowlFramesStringArr = allFramesString.split(TenPinBowlingConstants.FRAME_BOUNDARY);
        for (int i = 0; i < bowlFramesStringArr.length; i++) {
            BowlingFrame newFrame = new BowlingFrame();
            newFrame.initializeFrameWithString(bowlFramesStringArr[i]);
            addBowlingFrame(newFrame);
        }

        //The player has bonus balls
        if (wholeGameStringArray.length > 1) {
            String bonusBallsString = wholeGameStringArray[1];
            setUpBonusBallsWithString(bonusBallsString);
        }
    }

    /**
     * Set up the bonus balls information of this game using the given string.
     *
     * @param bonusBallsString
     */
    public void setUpBonusBallsWithString(String bonusBallsString) {
        String[] allBonusBalls = bonusBallsString.split("");

        for (int i = 0; i < allBonusBalls.length; i++) {
            BowlingBall newBonusBall = new BowlingBall();
            if (allBonusBalls[i].equals(TenPinBowlingConstants.STRIKE)) {
                newBonusBall.setBallPoint(10);
            } else if (allBonusBalls[i].equals(TenPinBowlingConstants.SPARE)) {
                //If we see the spare symbol, it is obvious that there exists a first bonus ball
                //so the point of this ball is equal to (10 minus the first bonus ball's point)
                newBonusBall.setBallPoint(10 - bonusBalls.get(0).getBallPoint());
            } else {
                newBonusBall.setBallPoint(Integer.parseInt(allBonusBalls[i]));
            }
            throwBonusBall(newBonusBall);
        }
    }

    /**
     * Add a bowling frame to this game.
     * @param bowlingFrame
     */
    public void addBowlingFrame(BowlingFrame bowlingFrame) {
        allFramesPlayed.add(bowlingFrame);
        bowlingBalls.addAll(bowlingFrame.getBowlingBalls());
        updateGamePoints();
    }

    /**
     * Update the frame score of all frames played this game and the gameScore.
     */
    public void updateGamePoints() {
        // A greedy approach: we will run the algorithm backward.
        // This way, Strike and Spare frames will already know what the values of the next balls are.
        int newGameScore = 0;
        int lastBallIndex = bowlingBalls.size() - 1;

        // We start the algorithm with the last ball of the last frame
        int currentBallIndex = lastBallIndex - bonusBalls.size();


        for (int i = allFramesPlayed.size() - 1; i >= 0; i--) {
            if (allFramesPlayed.get(i).isStrikeScored() && (currentBallIndex < lastBallIndex - 1)) {
                // We need to make sure there exist two balls after the current ball to account for a Strike
                allFramesPlayed.get(i).setFrameScore(10 + bowlingBalls.get(currentBallIndex + 1).getBallPoint() + bowlingBalls.get(currentBallIndex + 2).getBallPoint());
            } else if (allFramesPlayed.get(i).isSpareScored() && (currentBallIndex < lastBallIndex)) {
                // We only need one ball to exist after the current ball to account for a Spare
                allFramesPlayed.get(i).setFrameScore(10 + bowlingBalls.get(currentBallIndex + 1).getBallPoint());
            }
            newGameScore += allFramesPlayed.get(i).getFrameScore();
            currentBallIndex -= allFramesPlayed.get(i).getBowlingBalls().size();
        }

        // Update the game score
        gameScore = newGameScore;
    }

    /**
     * Throw the input bowling ball as a bonus ball this game.
     * @param inputBowlingBall
     */
    public void throwBonusBall(BowlingBall inputBowlingBall) {
        bonusBalls.add(inputBowlingBall);
        bowlingBalls.add(inputBowlingBall);
        bonusPoint += inputBowlingBall.getBallPoint();
        updateGamePoints();
    }

    //endregion Public Interface

}
