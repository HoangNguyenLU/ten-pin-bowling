/**
 * Global constants that will not change during runtime.
 */
public final class TenPinBowlingConstants {

    /** Private constructor. This class is only for constants and static utility functions. */
    private TenPinBowlingConstants() { }

    //region String Constants

    /** Default string representation of a strike. */
    public static final String STRIKE = "X";

    /** Default string representation of a spare. */
    public static final String SPARE = "/";

    /** Default string representation of a miss. */
    public static final String MISS = "-";

    /** Default string representation of a frame boundary. */
    public static final String FRAME_BOUNDARY = "\\|";

    /** Default string representation of bonus balls. */
    public static final String BONUS_BALLS = "\\|\\|";

    //endregion String Constants

}
