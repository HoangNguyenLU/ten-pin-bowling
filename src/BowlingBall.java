/**
 * A thrown bowling ball in a frame, or a bonus ball.
 */
public class BowlingBall {

    //region Properties & Variables

    //Internal Variables
    private int ballPoint;

    //endregion Properties & Variables

    //region Constructor

    public BowlingBall() {
        ballPoint = 0;
    }

    public BowlingBall(int point) {
        ballPoint = point;
    }

    //endregion Constructor


    //region Public Interface

    public int getBallPoint() {
        return ballPoint;
    }

    public void setBallPoint(int ballPoint) {
        this.ballPoint = ballPoint;
    }

    //endregion Public Interface

}