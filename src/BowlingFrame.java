import java.util.ArrayList;

/**
 * A single frame in a bowling game.
 */
public class BowlingFrame {

    //region Properties & Variables

    //Internal variables
    private int frameScore;
    private boolean strikeScored;
    private boolean spareScored;
    private ArrayList<BowlingBall> ballsThrownThisFrame;

    //endregion Properties & Variables

    //region Constructor

    public BowlingFrame() {
        frameScore = 0;
        strikeScored = false;
        spareScored = false;
        ballsThrownThisFrame = new ArrayList<>();
    }

    //endregion Constructor

    //region Public Interface
    /**
     * Set up this BowlingFrame instance with the input string.
     * @param inputString
     */
    public void initializeFrameWithString(String inputString) {
        //If the length of the string is less than 2, the player has managed to score a Strike
        if (inputString.length() < 2) {
            BowlingBall strikeBall = new BowlingBall(10);
            throwBall(strikeBall);
        } else if (inputString.contains(TenPinBowlingConstants.SPARE)) {
            // The user has just scored a Spare
            String[] allBalls = inputString.split("");
            BowlingBall firstBall = new BowlingBall(Integer.parseInt(allBalls[0]));
            BowlingBall secondBall = new BowlingBall(10 - Integer.parseInt(allBalls[0]));
            throwBall(firstBall);
            throwBall(secondBall);
        } else {
            String[] allBalls = inputString.split("");
            for (int i = 0; i < allBalls.length; i++ ) {
                BowlingBall newBall;
                if (allBalls[i].equals(TenPinBowlingConstants.MISS)) {
                    //If the user misses, it is considered a 0-point ball
                    newBall = new BowlingBall(0);
                } else {
                    newBall = new BowlingBall(Integer.parseInt(allBalls[i]));
                }
                throwBall(newBall);
            }
        }
    }

    /**
     * Throws a ball in this frame and updates the ballsThrownThisFrame ArrayList,
     * as well as the score of the frame.
     * @param newBall
     */
    public void throwBall(BowlingBall newBall) {
        ballsThrownThisFrame.add(newBall);
        frameScore += newBall.getBallPoint();
        if (newBall.getBallPoint() == 10) {
            strikeScored = true;
        } else if (frameScore == 10 && ballsThrownThisFrame.size() == 2) {
            spareScored = true;
        }
    }

    /**
     * Return true if we score a Strike this frame.
     * @return true if we score a Strike this frame.
     */
    public boolean isStrikeScored() {
        return strikeScored;
    }

    /**
     * Return true if we score a Strike this frame.
     * @return true if we score a Spare this frame.
     */
    public boolean isSpareScored() {
        return spareScored;
    }

    /**
     * Returns the score of this frame.
     * @return frameScore.
     */
    public int getFrameScore() {
        return frameScore;
    }

    /**
     * Set the score for frame.
     * @param newFrameScore
     */
    public void setFrameScore(int newFrameScore) {
        this.frameScore = newFrameScore;
    }

    /**
     * Returns the ArrayList of all the balls thrown in this frame.
     * @return ballsThrownThisFrame
     */
    public ArrayList<BowlingBall> getBowlingBalls() {
        return ballsThrownThisFrame;
    }

    //endregion Public Interface

}
